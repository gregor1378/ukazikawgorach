(function ($) {

	new WOW().init();

	jQuery(window).load(function() {
		jQuery("#preloader").delay(100).fadeOut("slow");
		jQuery("#load").delay(100).fadeOut("slow");
		// init services popups

		$('#housesBtn').on('click', function(evt) {
			$('#service-houses.popup').show();
		});

		$('.popup-close').on('click', function(evt) {
			$(evt.currentTarget).parents('.popup').hide();
		})


	});

	// gallery
	jQuery(document).ready(function(e) {
		 jQuery('#my_carousel_ct').tsSlider({
			thumbs:'left',
			//width: 100,
			 height: 100,
			showText: true,
			autoplay: false, //5000,
			//imgWidth: 130,
			imgHeight: 80,
			imgMarginTop: 0,
			imgMarginLeft:0,
			squared: true,
			textSquarePosition: 4,
			textPosition: 'top',
			imgAlignment: 'left',
			textColor: 'ffffff',
			skin: 'ts-border-18',
			arrows:'ts-arrow-1',
			sliderHeight: '100%',
			effects:'fan',/*''-->default boostrap --> 'random', 'cubeH', 'cubeV', 'fade', 'sliceH', 'sliceV', 'slideH', 'slideV', 'scale',
		'blockScale', 'kaleidoscope', 'fan', 'blindH', 'blindV', 'fadein',  'blind','fallingbars', 'appear', 'fillin', 'explode', 'jumble',
		'risingbars', 'paint', 'diagonal', 'crunchingbars', 'slidein', 'scrollVert3d', 'scrollHorz3d', 'scrollVert',
		'scrollHorz' IE only support Fade */
			titleBold: 'true',
			titleItalic: 'true',
			textBold: 'true',
			textItalic: 'true',
			textWidth: 50,
			background_sld:'',
			background_caption:''
		});
	});


	//jQuery to collapse the navbar on scroll
	$(window).scroll(function() {
		if ($(".navbar").offset().top > 50) {
			$(".navbar-fixed-top").addClass("top-nav-collapse");
		} else {
			$(".navbar-fixed-top").removeClass("top-nav-collapse");
		}
	});

	//jQuery for page scrolling feature - requires jQuery Easing plugin
	$(function() {
		$('.navbar-nav li a').bind('click', function(event) {
			var $anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top
			}, 1500, 'easeInOutExpo');
			event.preventDefault();
		});
		$('.page-scroll a').bind('click', function(event) {
			var $anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top
			}, 1500, 'easeInOutExpo');
			event.preventDefault();
		});
	});




})(jQuery);
